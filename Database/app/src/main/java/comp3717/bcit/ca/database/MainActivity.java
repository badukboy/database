package comp3717.bcit.ca.database;

import android.app.Activity;
import android.app.LoaderManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;

public class MainActivity
    extends Activity
{
    private static final String TAG = MainActivity.class.getName();
    private NamesOpenHelper namesOpenHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        final LoaderManager manager;

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        namesOpenHelper = NamesOpenHelper.getInstance(getApplicationContext());

        init();
        display();
    }

    private void init()
    {
        final SQLiteDatabase db;
        final long           numEntries;

        db = namesOpenHelper.getWritableDatabase();
        numEntries = namesOpenHelper.getNumberOfNames(db);

        if(numEntries == 0)
        {
            db.beginTransaction();

            try
            {
                namesOpenHelper.insertName(db,
                                           "D'Arcy");
                namesOpenHelper.insertName(db,
                                           "Medhat");
                namesOpenHelper.insertName(db,
                                           "Albert");
                namesOpenHelper.insertName(db,
                                           "Jason");
                db.setTransactionSuccessful();
            }
            finally
            {
                db.endTransaction();
            }
        }

        db.close();
    }

    private void display()
    {
        final SQLiteDatabase db;
        final Cursor         cursor;

        db     = namesOpenHelper.getReadableDatabase();
        cursor = namesOpenHelper.getAllNames(db);

        while(cursor.moveToNext())
        {
            final String name;

            name = cursor.getString(0);
            Log.d(TAG, name);
        }

        cursor.close();
    }
}