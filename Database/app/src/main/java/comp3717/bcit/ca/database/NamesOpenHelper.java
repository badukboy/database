package comp3717.bcit.ca.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by darcy on 2016-10-16.
 */

public final class NamesOpenHelper
    extends SQLiteOpenHelper
{
    private static final String TAG = NamesOpenHelper.class.getName();
    private static final int SCHEMA_VERSION = 1;
    private static final String DB_NAME = "names.db";
    private static final String NAME_TABLE_NAME = "name";
    private static final String ID_COLUMN_NAME = "_id";
    private static final String NAME_COLUMN_NAME = "name";
    private static NamesOpenHelper instance;
    private final Context context;

    private NamesOpenHelper(final Context ctx)
    {
        super(ctx, DB_NAME, null, SCHEMA_VERSION);

        context = ctx;
    }

    public synchronized static NamesOpenHelper getInstance(Context context)
    {
        if(instance == null)
        {
            instance = new NamesOpenHelper(context.getApplicationContext());
        }

        return instance;
    }

    @Override
    public void onConfigure(final SQLiteDatabase db)
    {
        super.onConfigure(db);

        setWriteAheadLoggingEnabled(true);
        db.setForeignKeyConstraintsEnabled(true);
    }

    @Override
    public void onCreate(final SQLiteDatabase db)
    {
        final String CREATE_NAME_TABLE;

        CREATE_NAME_TABLE = "CREATE TABLE IF NOT EXISTS "  + NAME_TABLE_NAME + " ( " +
                            ID_COLUMN_NAME   + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                            NAME_COLUMN_NAME + " TEXT NOT NULL)";
        db.execSQL(CREATE_NAME_TABLE);
    }

    @Override
    public void onUpgrade(final SQLiteDatabase db,
                          final int oldVersion,
                          final int newVersion)
    {
    }

    public long getNumberOfNames(final SQLiteDatabase db)
    {
        final long numEntries;

        numEntries = DatabaseUtils.queryNumEntries(db, NAME_TABLE_NAME);

        return (numEntries);
    }

    public void insertName(final SQLiteDatabase db,
                           final String         name)
    {
        final ContentValues contentValues;

        contentValues = new ContentValues();
        contentValues.put(NAME_COLUMN_NAME, name);
        db.insert(NAME_TABLE_NAME, null, contentValues);
    }

    public Cursor getAllNames(final SQLiteDatabase db)
    {
        final String selectQuery;
        final Cursor cursor;

        selectQuery = "SELECT * FROM " + NAME_TABLE_NAME;
        cursor = db.query(NAME_TABLE_NAME,
                          new String[]
                          {
                              NAME_COLUMN_NAME,
                          },
                          null,     // selection, null = *
                          null,     // selection args (String[])
                          null,     // group by
                          null,     // having
                          null,     // order by
                          null);    // limit

        return (cursor);
    }
}
